extends Node3D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var movement : float = 0
	if Input.is_action_pressed("ui_up"):
		movement -= 1
	if Input.is_action_pressed("ui_down"):
		movement += 1
	translate(movement * delta * Vector3(0,0,1))
	

	if Input.is_action_pressed("ui_right"):
		rotate_y(-delta)
	if Input.is_action_pressed("ui_left"):
		rotate_y(delta)
		
