extends Node3D


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_x(delta)
	rotate_y(delta * .5)
	rotate_z(delta * .25)
