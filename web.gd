extends Node

@export var size : Vector2i = Vector2i(64, 64)

var prev_frame : PackedByteArray = []

var started : bool = false

func get_coord_id(x : int, y : int) -> String:
	return "X" + str(x) + "Y" + str(y)

func _ready():
	JavaScriptBridge.eval("""
		var styleSheet = document.createElement("style");
		styleSheet.innerText = "input {margin: 0;} th {padding: 0;} canvas {margin: 0 auto;}";
		document.head.appendChild(styleSheet);
	""")
	

func start() -> void:
	get_viewport().size = size
	size = get_viewport().size
	
	# Set frame cache
	prev_frame.resize(size.y * size.x)

	# Create html checkbox table
	var table : String = ""
	
	for y : int in size.y:
		table += "<tr>"
		for x : int in size.x:
			table += "<th> <input type='checkbox' id='%s'> </th>" % get_coord_id(x, y)
		table += "</tr>"
			
	JavaScriptBridge.eval("""
		// Hide canvas
		document.getElementById("canvas").style.position = "absolute";
		document.getElementById("canvas").style.left = "-500px";
	
		// Create table
		let elemDiv = document.createElement('div');
		elemDiv.innerHTML = "<table> %s </table>";
		document.body.appendChild(elemDiv);
		
		
		document.getElementById("canvas").focus();
		
		document.body.addEventListener("click", () => {
			document.getElementById("canvas").focus();
		});
	""" % table)
	
	$Button.modulate = Color.TRANSPARENT
	started = true

func _process(_delta : float):
	if not started:
		return
		
	var image : Image = get_viewport().get_texture().get_image()
	var js : String = ""
	
	for y : int in size.y:
		for x : int in size.x:
			var lum : float = image.get_pixel(x, y).srgb_to_linear().get_luminance() 
			var binary : int = 1 if lum > .4 else 0
			# Only add JS if pixel changes
			if prev_frame.decode_u8((y * size.x) + x) != binary:
				js += "document.getElementById('%s').checked = %s;" % [get_coord_id(x, y) , str(lum > .4)]
				prev_frame.encode_u8((y * size.x) + x, binary)

	JavaScriptBridge.eval(js)
